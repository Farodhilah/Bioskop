  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <title> CINEMA 25 </title>
      <link rel="stylesheet" href="<?=base_url()?>asset/a.css">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


  <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/css/bootstrap.css">
<link rel="stylesheet" href="<?=base_url()?>asset/font-awesome/css/font-awesome.min.css">
  <script type="text/javascript" src="<?=base_url()?>asset/<?=base_url()?>asset/js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>asset/<?=base_url()?>asset/js/bootstrap.js"></script>
<link rel="icon" href="<?=base_url()?>asset/image/cin.jpg">
    </head>

  <body>

             <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
    
      <div class="navbar-header">
      <a class="navbar-brand" href="#"><img src="<?=base_url()?>asset/image/cin.jpg" style="height: 60px;"></a>
      </div>
      
    <div class="navbar-header" >
    
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
    
    
    
    </div>
  
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      <ul class="nav navbar-nav">
      <li><a href="<?=base_url()?>index.php/Bioskop/home"><span class="glyphicon glyphicon-share-home"></span> HOME</a></li>
        <li><a href="<?=base_url()?>index.php/Bioskop/jadwal"><span class="glyphicon glyphicon-play"></span>Jadwal</a></li>
    <li><a href="<?=base_url()?>index.php/Bioskop/kritik"><span class="glyphicon glyphicon-user"></span> Kritik</a></li>

    <li><a href="<?=base_url()?>index.php/Bioskop/tm_film"><span class="glyphicon glyphicon-saved"></span> TICKET BOOKING</a></li>
    <li><a href="<?=base_url()?>/index.php/cart">
              <span class="fa fa-shopping-cart"></span>
              <span class="label label-success">
                <?= $this->cart->total_items();?>
              </span>
            </a>
            </li>
            <li><a href="<?=base_url()?>index.php/pesanan"><span class="glyphicon glyphicon-list-alt"></span> HISTORY</a></li>
          
    <li><a href="<?=base_url()?>index.php/Bioskop/logout"><span class="glyphicon glyphicon-user"></span>
<?php if($this->session->userdata('login')==TRUE){
  echo "LOGOUT";
}else{
  echo "LOGIN";
}?>
    </a></li>
    
    <li><a href="#"><span class="glyphicon glyphicon-user"><?php echo $this->session->userdata('username');?></span></a></li>



    <h2 style="padding-left: 530px; color: white; font-family: cursive"><br>CINEMA 25 </h2>
    </ul>

        
    
  </div>
    </div>
  </nav>

      <?php 
        $this->load->view($konten);
       ?>
           
              

       </div>
        </div>

        <div id="box5">
        </div>

        <footer>
          <section id="contact" >
<div class="container">
<div class="row text-center header animate-in" data-anim-type="fade-in-up">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

<h3>Contact CINEMA 25 </h3>
<hr />

</div>
</div>

<div class="row animate-in" data-anim-type="fade-in-up">

<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="contact-wrapper">
<h3>Thank you<br> Jangan Lupa Tersenyum<br>:)))</h3>


</div>

</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="contact-wrapper">
<h3>Quick Contact</h3>
<h4><strong>Email : </strong> Cinema25@gmail.com </h4>
<h4><strong>Call : </strong> +628732469232 </h4>
</div>

</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="contact-wrapper">
<h3>Address : </h3>
<h4>MALANG, CINEMA 25 90/7B </h4>
<h4>INDONESIA</h4>
</div>

</div>

</div>


</div>
</section>
        </footer>








    </body>
  </html>
<script>
var acuan=$(".carousel").offset().top;
var menu=function() {
      var scrol=$(window).scrollTop();
        if(scrol>acuan){
          $(".navbar-inverse").addClass("berubah-menu");}
        else{
            $(".navbar-inverse").removeClass("berubah-menu");

      }
}
menu();
$(window).scroll(function(){
      menu();
});
</script>
